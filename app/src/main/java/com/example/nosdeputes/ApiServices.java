package com.example.nosdeputes;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nosdeputes.models.Deputy;
import com.example.nosdeputes.models.Responsability;
import com.example.nosdeputes.models.Vote;
import com.example.nosdeputes.observers.SearchObserver;
import com.example.nosdeputes.observers.VoteObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ApiServices {

    private static final String URL_API_SEARCH = "https://www.nosdeputes.fr/recherche/";
    private static final String URL_END_FORMAT_JSON = "?format=json";
    private static final String URL_AVATAR = "https://www.nosdeputes.fr/depute/photo/";
    private static final String URL_API_VOTES = "https://www.nosdeputes.fr/";
    private static final String URL_END_API_VOTES = "/votes/json";

    public static void searchRequest(Context context, String search, SearchObserver listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(URL_API_SEARCH + search + URL_END_FORMAT_JSON,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                if (object.getString("document_type").equals("Parlementaire")) {
                                    getDeputyInfo(context, object.getString("document_url"), listener);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(request);
    }

    public static void getDeputyInfo(Context context, String urlInfoDeputy, SearchObserver listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(urlInfoDeputy,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObjectDeputy = jsonObject.getJSONObject("depute");
                            Deputy deputy = new Deputy(jsonObjectDeputy.getInt("id"),
                                    jsonObjectDeputy.getInt("num_circo"),
                                    jsonObjectDeputy.getString("prenom"),
                                    jsonObjectDeputy.getString("nom_de_famille"),
                                    jsonObjectDeputy.getString("num_deptmt"),
                                    jsonObjectDeputy.getString("nom_circo"));

//                            TODO: rajouter l'email du député
//                            JSONArray emailsArray = jsonObjectDeputy.getJSONArray("emails");


                            JSONArray collaborateursArray = jsonObjectDeputy.getJSONArray("collaborateurs");
                            ArrayList<String> deputyCollaborators = new ArrayList<>();
                            for (int i = 0; i < collaborateursArray.length(); i++) {
                                JSONObject collaborator = collaborateursArray.getJSONObject(i);
                                deputyCollaborators.add(collaborator.getString("collaborateur"));
                            }
                            deputy.setCollaborateurs(deputyCollaborators);


                            JSONArray responsabilitesArray = jsonObjectDeputy.getJSONArray("responsabilites");
                            ArrayList<Responsability> deputyResponsabilities = new ArrayList<>();
                            for (int i = 0; i < responsabilitesArray.length(); i++) {
                                JSONObject responsability = responsabilitesArray.getJSONObject(i);
                                JSONObject jsonObjectResponsability = responsability.getJSONObject("responsabilite");
                                Responsability deputyResponsability = new Responsability(
                                        jsonObjectResponsability.getString("organisme"),
                                        jsonObjectResponsability.getString("fonction"),
                                        jsonObjectResponsability.getString("debut_fonction")
                                );
                                deputyResponsabilities.add(deputyResponsability);
                            }
                            deputy.setResponsabilities(deputyResponsabilities);


                            JSONObject groupeObject = jsonObjectDeputy.getJSONObject("groupe");
                            deputy.setFonction(groupeObject.getString("fonction"));
                            deputy.setOrganisme(groupeObject.getString("organisme"));
                            deputy.setDebutFonction(groupeObject.getString("debut_fonction"));


                            deputy.setSexe(jsonObjectDeputy.getString("sexe"));
                            deputy.setDateNaissance(jsonObjectDeputy.getString("date_naissance"));
                            deputy.setLieuNaissance(jsonObjectDeputy.getString("lieu_naissance"));
                            deputy.setDateDebutMandat(jsonObjectDeputy.getString("mandat_debut"));
                            deputy.setPlaceHemicycle(jsonObjectDeputy.getString("place_en_hemicycle"));
                            deputy.setProfession(jsonObjectDeputy.getString("profession"));
                            deputy.setTwitter(jsonObjectDeputy.getString("twitter"));
                            listener.onReceiveDeputyInfo(deputy);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(request);
    }

    public static void loadDeputyAvatar(Context context, String deputyName, final ImageView imageView) {
        RequestQueue queue = Volley.newRequestQueue(context);
        ImageRequest request = new ImageRequest(URL_AVATAR + deputyName + "/160",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imageView.setImageResource(android.R.drawable.ic_menu_gallery);
                    }
                });
        queue.add(request);
    }

    public static void votesRequest(Context context, String deputyName, VoteObserver listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(URL_API_VOTES + deputyName + URL_END_API_VOTES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("votes");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                JSONObject objectVote = object.getJSONObject("vote");
                                Vote vote = new Vote();
                                vote.setPosition(objectVote.getString("position"));
                                vote.setPositionGroupe(objectVote.getString("position_groupe"));
                                vote.setParDelegation((objectVote.getString("par_delegation").equals("false") ? false : true));
                                vote.setParlementaireGroupeAcronyme(objectVote.getString("parlementaire_groupe_acronyme"));
                                JSONObject objectScrutin = objectVote.getJSONObject("scrutin");
                                vote.setDate(objectScrutin.getString("date"));
                                vote.setType(objectScrutin.getString("type"));
                                vote.setSort(objectScrutin.getString("sort"));
                                vote.setTitre(objectScrutin.getString("titre"));
                                vote.setNombreVotants(objectScrutin.getInt("nombre_votants"));
                                vote.setNombrePour(objectScrutin.getInt("nombre_pours"));
                                vote.setNombreContre(objectScrutin.getInt("nombre_contres"));
                                vote.setNombreAbst(objectScrutin.getInt("nombre_abstentions"));
//                                JSONArray arrayDemandeurs = objectScrutin.getJSONArray("demandeurs");
//                                JSONObject objectDemandeur = arrayDemandeurs.getJSONObject(0);
//                                vote.setDemandeur(objectDemandeur.getString("demandeur"));
                                listener.onReceiveVoteInfo(vote);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(request);
    }
}
