package com.example.nosdeputes.observers;

import com.example.nosdeputes.models.Deputy;

public interface SearchObserver {
    public void onReceiveDeputyInfo(Deputy deputy);
}
