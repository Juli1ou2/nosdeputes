package com.example.nosdeputes.observers;

import com.example.nosdeputes.models.Vote;

public interface VoteObserver {
    public void onReceiveVoteInfo(Vote vote);
}
