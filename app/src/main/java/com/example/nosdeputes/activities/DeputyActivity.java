package com.example.nosdeputes.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.nosdeputes.ApiServices;
import com.example.nosdeputes.R;
import com.example.nosdeputes.adapters.CollaboratorsAdapter;
import com.example.nosdeputes.adapters.ResponsabilitiesAdapter;
import com.example.nosdeputes.adapters.VotesAdapter;
import com.example.nosdeputes.models.Deputy;
import com.example.nosdeputes.models.Vote;
import com.example.nosdeputes.observers.VoteObserver;

import java.util.ArrayList;
import java.util.Collections;

public class DeputyActivity extends AppCompatActivity implements View.OnClickListener, VoteObserver {
    private TextView textViewDetailDeputyName, textViewDetailDeputyInfosNaissance, textViewDetailDeputyDateMandat,
            textViewDetailDeputyGroupe, textViewDetailDeputyPlaceHemi, textViewDetailDeputyProfession, textViewDetailDeputyTwitter;
    private ImageView imageViewDetailDeputyProfil;
    private ImageButton twitterButton;
    private Deputy deputy;
    private ArrayList<Vote> votes;
    private ResponsabilitiesAdapter responsabilitiesAdapter;
    private CollaboratorsAdapter collaboratorsAdapter;
    private VotesAdapter votesAdapter;
    private ListView listViewResponsabilities, listViewCollaborators, listViewVotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deputy_details);

        textViewDetailDeputyName = findViewById(R.id.textViewDetailDeputyName);
        textViewDetailDeputyInfosNaissance = findViewById(R.id.textViewDetailDeputyInfosNaissance);
        textViewDetailDeputyDateMandat = findViewById(R.id.textViewDetailDeputyDateMandat);
        textViewDetailDeputyGroupe = findViewById(R.id.textViewDetailDeputyGroupe);
        textViewDetailDeputyPlaceHemi = findViewById(R.id.textViewDetailDeputyPlaceHemi);
        textViewDetailDeputyProfession = findViewById(R.id.textViewDetailDeputyProfession);
        textViewDetailDeputyTwitter = findViewById(R.id.textViewDetailDeputyTwitter);
        twitterButton = findViewById(R.id.twitterButton);
        twitterButton.setOnClickListener(this);
        imageViewDetailDeputyProfil = findViewById(R.id.imageViewDetailDeputyProfil);
        listViewResponsabilities = findViewById(R.id.listViewResponsabilities);
        listViewCollaborators = findViewById(R.id.listViewCollaborators);
        listViewVotes = findViewById(R.id.listViewVotes);

        votes = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        deputy = (Deputy) getIntent().getSerializableExtra("deputy");
        textViewDetailDeputyName.setText(String.format("%s %s", deputy.getFirstName(), deputy.getLastName()));
        textViewDetailDeputyInfosNaissance.setText(String.format(deputy.getInfosNaissance()));
        textViewDetailDeputyDateMandat.setText(deputy.getInfosDateMandat());
        textViewDetailDeputyGroupe.setText(deputy.getInfosGroupe());
        textViewDetailDeputyPlaceHemi.setText(deputy.getInfosHemi());
        textViewDetailDeputyProfession.setText(deputy.getInfosProfession());
        textViewDetailDeputyTwitter.setText(deputy.getInfosTwitter());
        ApiServices.loadDeputyAvatar(this, deputy.getNameForAvatar(), imageViewDetailDeputyProfil);
        responsabilitiesAdapter = new ResponsabilitiesAdapter(deputy.getResponsabilities(), this);
        listViewResponsabilities.setAdapter(responsabilitiesAdapter);
        collaboratorsAdapter = new CollaboratorsAdapter(deputy.getCollaborateurs(), this);
        listViewCollaborators.setAdapter(collaboratorsAdapter);

        if (deputy.getTwitter().isEmpty()) {
            twitterButton.setActivated(false);
            twitterButton.setVisibility(View.INVISIBLE);
        }

        ApiServices.votesRequest(this, deputy.getFirstName().toLowerCase() + '-' + deputy.getLastName().toLowerCase(), this);
        votesAdapter = new VotesAdapter(votes, this);
        listViewVotes.setAdapter(votesAdapter);
    }

    @Override
    public void onClick(View view) {
        ImageButton selectedButton = (ImageButton) view;
        if (selectedButton.equals(twitterButton)) {
            Uri twitterPage = Uri.parse("https://www.twitter.com/" + deputy.getTwitter());
            Intent twitterIntent = new Intent(Intent.ACTION_VIEW, twitterPage);
            startActivity(twitterIntent);
        }
    }

    @Override
    public void onReceiveVoteInfo(Vote vote) {
        if(!votes.contains(vote)){
            votes.add(vote);
            Collections.reverse(votes);
            votesAdapter.setVotes(votes);
            votesAdapter.notifyDataSetChanged();
        }
    }
}