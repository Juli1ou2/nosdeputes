package com.example.nosdeputes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.example.nosdeputes.ApiServices;
import com.example.nosdeputes.R;
import com.example.nosdeputes.observers.SearchObserver;
import com.example.nosdeputes.adapters.DeputiesAdapter;
import com.example.nosdeputes.models.Deputy;

import java.io.Serializable;
import java.util.ArrayList;

public class  MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SearchObserver, AdapterView.OnItemClickListener {
    private SearchView searchView;
    private ListView listView;
    private DeputiesAdapter deputiesAdapter;
    private ArrayList<Deputy> deputies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchView = findViewById(R.id.searchViewMain);
        searchView.setOnQueryTextListener(this);
        listView = findViewById(R.id.listViewMain);
        deputies = new ArrayList<>();
        deputiesAdapter = new DeputiesAdapter(deputies, this);
        listView.setAdapter(deputiesAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        deputies = new ArrayList<>();
        ApiServices.searchRequest(this, query, this);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onReceiveDeputyInfo(Deputy deputy) {
        if(!deputies.contains(deputy)){
            deputies.add(deputy);
            deputiesAdapter.setDeputies(deputies);
            deputiesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(MainActivity.this, DeputyActivity.class);
        intent.putExtra("deputy", (Serializable) deputies.get(i));
        startActivity(intent);
    }
}