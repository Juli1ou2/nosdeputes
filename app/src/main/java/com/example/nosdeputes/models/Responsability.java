package com.example.nosdeputes.models;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Locale;

public class Responsability implements Serializable {
    private String organisme, fonction, debutFonction;

    public Responsability(String organisme, String fonction, String debutFonction) {
        this.organisme = organisme;
        this.fonction = fonction;
        this.debutFonction = debutFonction;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getDebutFonction() {
        return debutFonction;
    }

    public void setDebutFonction(String debutFonction) {
        this.debutFonction = debutFonction;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.FRANCE, "● %s de %s depuis le %s", this.getFonction(),
                this.getOrganisme(), this.getDebutFonction());
    }
}
