package com.example.nosdeputes.models;

public class Vote {
    private String date, type, sort, titre, position, positionGroupe, parlementaireGroupeAcronyme, demandeur;
    private boolean parDelegation;
    private int nombreVotants, nombrePour, nombreContre, nombreAbst;

    public Vote() {}

    public String getEmojiFromValue(String value) {
        switch(value){
            case "pour":
                return "🟢";
            case "contre":
                return "🔴";
            case "abstention":
                return "🟡";
            case "adopté":
                return "✅";
            case "rejeté":
                return "❌";
            default:
                return "❔";
        }
    }

    public String getInfosDateAndSort() {
        return String.format("%s — %s", this.getDate(), this.getSort());
    }

    public String getInfosPosition() {
        String parDelegation = (this.isParDelegation()) ? " (par délégation)" : "";
        return String.format("▸ vote%s : %s — vote %s : %s",
                parDelegation,
                this.getEmojiFromValue(this.getPosition()),
                this.getParlementaireGroupeAcronyme(),
                this.getEmojiFromValue(this.getPositionGroupe()));
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionGroupe() {
        return positionGroupe;
    }

    public void setPositionGroupe(String positionGroupe) {
        this.positionGroupe = positionGroupe;
    }

    public String getParlementaireGroupeAcronyme() {
        return parlementaireGroupeAcronyme;
    }

    public void setParlementaireGroupeAcronyme(String parlementaireGroupeAcronyme) {
        this.parlementaireGroupeAcronyme = parlementaireGroupeAcronyme;
    }

    public String getDemandeur() {
        return demandeur;
    }

    public void setDemandeur(String demandeur) {
        this.demandeur = demandeur;
    }

    public boolean isParDelegation() {
        return parDelegation;
    }

    public void setParDelegation(boolean parDelegation) {
        this.parDelegation = parDelegation;
    }

    public int getNombreVotants() {
        return nombreVotants;
    }

    public void setNombreVotants(int nombreVotants) {
        this.nombreVotants = nombreVotants;
    }

    public int getNombrePour() {
        return nombrePour;
    }

    public void setNombrePour(int nombrePour) {
        this.nombrePour = nombrePour;
    }

    public int getNombreContre() {
        return nombreContre;
    }

    public void setNombreContre(int nombreContre) {
        this.nombreContre = nombreContre;
    }

    public int getNombreAbst() {
        return nombreAbst;
    }

    public void setNombreAbst(int nombreAbst) {
        this.nombreAbst = nombreAbst;
    }
}
