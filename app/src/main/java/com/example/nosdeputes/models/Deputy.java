package com.example.nosdeputes.models;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;

public class Deputy implements Serializable {
    int id, numCirco;
    private String firstName, lastName, department, nameCirco;
    private String sexe, dateNaissance, lieuNaissance, dateDebutMandat, fonction, organisme, debutFonction,
            profession, placeHemicycle, twitter;
    private ArrayList<String> collaborateurs, emails;
    private ArrayList<Responsability> responsabilities;

    public Deputy(int id, int numCirco, String firstName, String lastName, String department, String nameCirco) {
        this.id = id;
        this.numCirco = numCirco;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.nameCirco = nameCirco;
    }

    public Deputy(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getDateDebutMandat() {
        return dateDebutMandat;
    }

    public void setDateDebutMandat(String dateDebutMandat) {
        this.dateDebutMandat = dateDebutMandat;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public String getDebutFonction() {
        return debutFonction;
    }

    public void setDebutFonction(String debutFonction) {
        this.debutFonction = debutFonction;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getPlaceHemicycle() {
        return placeHemicycle;
    }

    public void setPlaceHemicycle(String placeHemicycle) {
        this.placeHemicycle = placeHemicycle;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public ArrayList<String> getCollaborateurs() {
        return collaborateurs;
    }

    public void setCollaborateurs(ArrayList<String> collaborateurs) {
        this.collaborateurs = collaborateurs;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }

    public ArrayList<Responsability> getResponsabilities() {
        return responsabilities;
    }

    public void setResponsabilities(ArrayList<Responsability> responsabilities) {
        this.responsabilities = responsabilities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumCirco() {
        return numCirco;
    }

    public void setNumCirco(int numCirco) {
        this.numCirco = numCirco;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getNameCirco() {
        return nameCirco;
    }

    public void setNameCirco(String nameCirco) {
        this.nameCirco = nameCirco;
    }

    public String getNameForAvatar() {
        return firstName.replace(' ', '-').toLowerCase() + '-' +
                lastName.replace(' ', '-').toLowerCase();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Deputy d = (Deputy) obj;
        return id == d.getId();
    }

    public String getInfosNaissance() {
        String sexe = (this.getSexe().equals("H")) ? "♂️ né" : "♀️ née";
        return String.format("%s le %s à %s", sexe, this.getDateNaissance(), this.getLieuNaissance());
    }

    public String getInfosDateMandat() {
        return String.format("◆ Début du mandat : %s", this.getDateDebutMandat());
    }

    public String getInfosGroupe() {
        return String.format("◆ %s de %s depuis le %s", this.getFonction(), this.getOrganisme(), this.getDebutFonction());
    }

    public String getInfosHemi() {
        return String.format("◆ Place dans l'Hémicycle : %s", this.getPlaceHemicycle());
    }

    public String getInfosProfession() {
        return String.format("◆ Profession : %s", this.getProfession());
    }

    public String getInfosTwitter() {
        if (!this.getTwitter().isEmpty()) {
            return String.format("◆ Compte twitter :  %s", this.getTwitter());
        } else {
            return "- Aucun compte twitter";
        }
    }
}
