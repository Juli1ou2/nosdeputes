package com.example.nosdeputes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nosdeputes.ApiServices;
import com.example.nosdeputes.R;
import com.example.nosdeputes.models.Deputy;

import java.util.ArrayList;
import java.util.Locale;

public class DeputiesAdapter extends BaseAdapter {
    private ArrayList<Deputy> deputies;
    private final Context context;

    public DeputiesAdapter(ArrayList<Deputy> deputies, Context context) {
        this.deputies = deputies;
        this.context = context;
    }

    public void setDeputies(ArrayList<Deputy> deputies) {
        this.deputies = deputies;
    }

    @Override
    public int getCount() {
        return deputies.size();
    }

    @Override
    public Object getItem(int i) {
        return deputies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return deputies.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_deputy, viewGroup, false);
        }
        TextView textViewName = view.findViewById(R.id.textViewItemDeputyName);
        textViewName.setText(String.format("%s %s", deputies.get(i).getFirstName(), deputies.get(i).getLastName()));
        TextView textViewCirco = view.findViewById(R.id.textViewItemDeputyCirco);
        textViewCirco.setText(String.format(Locale.FRANCE, "%s %s, %d%s circonscription",
                deputies.get(i).getDepartment(),
                deputies.get(i).getNameCirco(),
                deputies.get(i).getNumCirco(),
                deputies.get(i).getNumCirco() == 1 ? "ère" : "ème"));

        ImageView imageView = view.findViewById(R.id.imageViewItemDeputy);
        ApiServices.loadDeputyAvatar(context, deputies.get(i).getNameForAvatar(), imageView);
        return view;
    }
}
