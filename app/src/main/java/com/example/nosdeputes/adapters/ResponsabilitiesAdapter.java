package com.example.nosdeputes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nosdeputes.R;
import com.example.nosdeputes.models.Responsability;

import java.util.ArrayList;

public class ResponsabilitiesAdapter extends BaseAdapter {
    private ArrayList<Responsability> responsabilities;
    private final Context context;

    public ResponsabilitiesAdapter(ArrayList<Responsability> responsabilities, Context context) {
        this.responsabilities = responsabilities;
        this.context = context;
    }

    public void setResponsabilities(ArrayList<Responsability> responsabilities) {
        this.responsabilities = responsabilities;
    }

    @Override
    public int getCount() {
        return responsabilities.size();
    }

    @Override
    public Object getItem(int i) {
        return responsabilities.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_responsability, viewGroup, false);
        }
        TextView textViewItemResponsability = view.findViewById(R.id.textViewItemResponsability);
        textViewItemResponsability.setText(responsabilities.get(i).toString());
        return view;
    }
}
