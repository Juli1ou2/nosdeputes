package com.example.nosdeputes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nosdeputes.R;

import java.util.ArrayList;

public class CollaboratorsAdapter extends BaseAdapter {
    private ArrayList<String> collaborators;
    private final Context context;

    public CollaboratorsAdapter(ArrayList<String> collaborators, Context context) {
        this.collaborators = collaborators;
        this.context = context;
    }

    public void setCollaborators(ArrayList<String> collaborators) {
        this.collaborators = collaborators;
    }

    @Override
    public int getCount() {
        return collaborators.size();
    }

    @Override
    public Object getItem(int i) {
        return collaborators.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_collaborator, viewGroup, false);
        }
        TextView textViewItemCollaborator = view.findViewById(R.id.textViewItemCollaborator);
        textViewItemCollaborator.setText(collaborators.get(i));
        return view;
    }
}
