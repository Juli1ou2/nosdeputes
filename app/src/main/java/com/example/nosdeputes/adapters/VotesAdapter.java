package com.example.nosdeputes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.nosdeputes.R;
import com.example.nosdeputes.models.Vote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.StringJoiner;

public class VotesAdapter extends BaseAdapter {
    private ArrayList<Vote> votes;
    private final Context context;

    public VotesAdapter(ArrayList<Vote> votes, Context context) {
        this.votes = votes;
        this.context = context;
    }

    public void setVotes(ArrayList<Vote> votes) {
        this.votes = votes;
    }

    @Override
    public int getCount() {
        return votes.size();
    }

    @Override
    public Object getItem(int position) {
        return votes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_vote, viewGroup, false);
        }
        TextView textViewItemVoteDateAndSort = view.findViewById(R.id.textViewItemVoteDateAndSort);
        textViewItemVoteDateAndSort.setText(votes.get(i).getInfosDateAndSort());
        TextView textViewItemVoteTitre = view.findViewById(R.id.textViewItemVoteTitre);
        textViewItemVoteTitre.setText(votes.get(i).getTitre());
        if (votes.get(i).getSort().equals("adopté")){
            textViewItemVoteTitre.setBackgroundColor(ContextCompat.getColor(context, R.color.vertClair));
            if (votes.get(i).getPosition().equals("pour")){
                textViewItemVoteTitre.setTextColor(ContextCompat.getColor(context, R.color.vertFonce));
            }
        } else {
            textViewItemVoteTitre.setBackgroundColor(ContextCompat.getColor(context, R.color.rougeClair));
            if (votes.get(i).getPosition().equals("contre")){
                textViewItemVoteTitre.setTextColor(ContextCompat.getColor(context, R.color.rougeFonce));
            }
        }
        TextView textViewItemVoteInfosPosition = view.findViewById(R.id.textViewItemVoteInfosPosition);
        textViewItemVoteInfosPosition.setText(votes.get(i).getInfosPosition());
        return view;
    }
}
