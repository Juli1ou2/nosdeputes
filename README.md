# NosDéputés

Application Android faite à l'école pour rechercher et voir les infos des députés français actifs.

- Android Studio
- Utilisation de l'[API nosdeputes](https://www.nosdeputes.fr "Voir le site web") ([documentation](https://github.com/regardscitoyens/nosdeputes.fr/blob/master/doc/api.md#r%C3%A9sultats-du-moteur-de-recherche "GitHub de l'API"))


## Captures d'écran

### Accueil
![Accueil](image.png)

### Recherche par nom
![Recherche par nom](image-1.png)

### Recherche par code postal
![Recherche par code postal](image-2.png)

### Vue détaillée du député avec infos, collaborateurs, responsabilités et votes (du plus récent)
![Vue détaillée du député avec infos, collaborateurs, responsabilités et votes (du plus récent)](image-3.png)